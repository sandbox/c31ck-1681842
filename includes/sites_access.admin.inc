<?php

/**
 * @file
 * Configuration forms for Sites Access.
 */

/**
 * Defines a configuration form for Sites access.
 */
function sites_access_config_form($form, &$form_state) {
  $form['sites_access_mode'] = array(
    '#type'             => 'radios',
    '#title'            => t('Path list mode'),
    '#options'          => array('blacklist' => t('Blacklist'), 'whitelist' => t('Whitelist')),
    '#default_value'    => variable_get('sites_access_mode', 'blacklist'),
    '#description'      => t('Blacklist allows access to all paths not on the list. Whitelist allows acccess to only paths on the list.'),
  );

  $form['sites_access_response'] = array(
    '#type'             => 'radios',
    '#title'            => t('Response'),
    '#options'          => array('redirect' => t('Redirect to main site counterpart'), 'access_denied' => t('Access denied')),
    '#default_value'    => variable_get('sites_access_response', 'redirect'),
    '#description'      => t('How to respond when a path matches.'),
  ); 

  $form['sites_access_paths'] = array(
    '#type'             => 'textarea',
    '#title'            => t('Paths'),
    '#default_value'    => variable_get('sites_access_paths', ''),
    '#description'      => t('Blacklist allows access to all paths not on the list. Whitelist allows acccess to only paths on the list.'),
  );

  $form = system_settings_form($form);
  // Add submit handler to clear the menu cache.
  $form['#submit'][] = 'sites_access_config_form_submit';
  return $form;
}

/**
 * Handles submit for sites_access_config_form().
 * 
 * Clears menu cache.
 */
function sites_access_config_form_submit() {
  menu_cache_clear_all();
}
